package xyz.gits.boot.common.core.constants;

/**
 * @author songyinyin
 * @date 2020/3/14 下午 08:16
 */
public interface SystemConstants {

    /**
     * 根机构id
     */
    String ORG_ROOT_ID = "0000";

    /**
     * 根资源id
     */
    String RESOURCE_ROOT_ID = "0";
}
