package xyz.gits.boot.common.core.enums;

/**
 * 登陆类型
 * @author songyinyin
 * @date 2020/5/7 下午3:06
 */
public enum LoginType {
    /**
     * 密码登陆
     */
    PASSWORD,
    /**
     * 扩展登陆
     */
    EXTEND,
    /**
     * gitee登陆
     */
    GITEE;

}
