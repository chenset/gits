package xyz.gits.boot.system.service;

import xyz.gits.boot.api.system.entity.UserRoleRel;
import xyz.gits.boot.common.core.basic.BasicService;

/**
 * <p>
 * 系统用户拥有角色关联表 服务类
 * </p>
 *
 * @author songyinyin
 * @date 2020-02-29
 */
public interface IUserRoleRelService extends BasicService<UserRoleRel> {

}
