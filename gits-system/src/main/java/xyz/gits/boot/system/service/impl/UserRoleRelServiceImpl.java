package xyz.gits.boot.system.service.impl;

import xyz.gits.boot.api.system.entity.UserRoleRel;
import xyz.gits.boot.common.core.basic.BasicServiceImpl;
import xyz.gits.boot.system.mapper.UserRoleRelMapper;
import xyz.gits.boot.system.service.IUserRoleRelService;

/**
 * <p>
 * 系统用户拥有角色关联表 服务实现类
 * </p>
 *
 * @author songyinyin
 * @date 2020-02-29
 */
public class UserRoleRelServiceImpl extends BasicServiceImpl<UserRoleRelMapper, UserRoleRel> implements IUserRoleRelService {

}
